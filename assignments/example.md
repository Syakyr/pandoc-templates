---
title: Generic Assignment
author: Syakyr Surani, A0154706E
date: 31 August 2018
...

# Question 1

## Part A

When we draw the constraints, we get the following graph[^1]:

[^1]: The graph is referenced from Tutorial 3, Q4.

![](figure/hw4qn1a.png){width=60%}\

We can see that to satisfy the 2 extra conditions, we would need to draw a line from (0,6) to (6,6), that is, to draw a line $x_2 \leq 6$.

## Part B

With the $x_2 \leq 6$ constraint, it is seen that (0,6) and (6,6) are degenerate and the solution to $-2x_1 + 3x_2 = 6$ and $-3x_1 - 2x_2 = -12$, which is $(\frac{24}{13},\frac{42}{13})$, is non-degenerate, where:

\begin{align*}
\text{(0,6) satisfies:} \\
x_2 &\geq 0 \\
-3x_1 - 2x_2 &\leq -12 \\
x_1 &\leq 6 \\
\text{(6,6) satisfies:} \\
-2x_1 + 3x_2 &\geq 6 \\
3x_1 - 2x_2 &\leq 6 \\
x_1 &\leq 6 \\
(\frac{24}{13},\frac{42}{13})\text{ satisfies:} \\
-2x_1 + 3x_2 &\geq 6 \\
-3x_1 - 2x_2 &\leq -12 \\
\end{align*}

## Part C

The answer is as follows:  

![](figure/hw4qn1a.png){width=60%}\

# Question 2

## Part A

$f(x)$ is convex if and only if:

$$f(\alpha{x} + (1 - \alpha)y) \leq \alpha{f(x)} + (1-\alpha)f(y)$$

However, we can show that $f(x)$ is not convex by using an example.

\begin{align*}
\intertext{Let: $x = (1,0), y = (0,1), \alpha = \frac{1}{2}$.}
f(\alpha{x} + (1 - \alpha)y)
&= f(\frac{1}{2}(1,0) + \frac{1}{2}(0,1)) \\
&= f(\frac{1}{2}, \frac{1}{2}) \\
&= \frac{1}{2} \\
\intertext{But:}
\alpha{f(x)} + (1-\alpha)f(y)
&= \frac{1}{2}f(1,0) + \frac{1}{2}f(0,1) \\
&= 0 \leq \frac{1}{2}
\end{align*}

Thus, we can see that $f(x)$ is not convex.

## Part B

For (P1), the constraints is that $x_1, x_2 \geq 0$. Thus, the lowest
value that either $x_1$ or $x_2$ can have is 0, thus the objective
value is 0.

For (P2), $t$ is unbounded towards the negative direction, thus the
objective value is $-\infty$.

# Question 3

To prove that the polyhedra,
$\{\bold{x}: \bold{Ax} \geq \bold{b} \mid
\bold{x}, \bold{b} \in \mathbb{R}^{n},
\bold{A} \in \mathbb{R}^{m \times n}\}$,
is convex, we can do the following:

\begin{align*}
\bold{A}(\alpha\bold{x} + (1 - \alpha)\bold{y})
&= \alpha\bold{Ax} + (1 - \alpha)\bold{Ay} \\
&\geq \alpha\bold{b} + (1 - \alpha)\bold{b} \\
&= \bold{b}(\alpha + 1 - \alpha) \\
&= \bold{b}
\end{align*}

# Question 4

We have:

\begin{align*}
\bar{\bold{c}}^T_B
&= \bold{c}^T_B - \bold{c}^T_B\bold{B}^{-1}\bold{A}_B \\
&= \bold{c}^T_B - \bold{c}^T_B\bold{B}^{-1}\bold{B} \\
&= \bold{c}^T_B - \bold{c}^T_B \\
&= \bold{0} = (0,...,0)
\end{align*}
