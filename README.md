# Pandoc Templates for Assignments and Reports

This is a repository to keep preferred templates for my assignments and reports.

## Requirements

You would need a few programs to keep it working:
- `pandoc`
- `pdflatex`
- $`\LaTeX`$

As I'm currently using MacOS, I use brew to install them.
Detailed instructions may come later when there is more time
allocated for that.

## How to install `pandoc` and $`\LaTaX`$

### For Linux (Ubuntu)

Install `pandoc` using conda:
```
conda create -n pandoc pandoc
```
To create a conda environment named `pandoc` and install `pandoc` inside it.

Install $`\LaTeX`$ using `apt`:

```
sudo apt install texlive-latex-base texlive-fonts-recommended cm-super texlive-pictures
```

### For MacOSX

Check this link [here](https://gist.github.com/arvindraghavan/298550869401add305f7).

```
brew install brew-cask
brew cask install mactex
brew install pandoc
```

## How to Use

Simple. To create a PDF from an MD file, the syntax would be:
> `make {basename}.pdf`

For example, to create a PDF from `example.md`, the syntax is:
> `make example.pdf`

Same goes for creating docx files:
> `make {basename}.docx`

For `presentation`, you make html slides instead:
> `make {basename}.html`

## Rebuilding the PDF/HTML File

You will need to remove the PDF/HTML file that was generated
previously in order to regenerate the file.

## Moving to Another Folder

Copy the folder of the template you want to use, and remove
the media files that you are not using (e.g. `example` files
and `figure` folder). All of them are important.

# Types of templates

- `assignments` - A simple template found online (original
  source to be confirmed)
- `reports` - Adapted and simplified from AMStat's format.
  Suitable for reports that doesn't require abstracts and
  bibliographies.
- `fullReports` - Using AMStat's format
- `presentation` - Uses [revealJS][revealjs] with mathJax to generate dynamic HTML slides.
  + Theme used is 'Night' and transition is 'zoom'. You can change these by looking at `-V theme=moon -V transition=zoom` and change accordingly.
  + Available transitions can be found [here][transitions], and the available themes can be seen at the next slide.
- `cheatSheet` - Derived from [here][cheatSheet], licensed by CC-BY-SA-4.0, same as this repository.

[revealjs]: https://revealjs.com/
[transitions]: https://revealjs.com/#/transitions
[cheatSheet]: https://github.com/idiv-biodiversity/pandoc-cheat-sheet
