\documentclass[11pt]{article}
\usepackage{hyperref}
\usepackage[margin=35mm]{geometry}

\usepackage{longtable}
\usepackage{booktabs}
\usepackage{tikz}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}

\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}

% Redefine \includegraphics so that, unless explicit options are
% given, the image width will not exceed the width or the height of the page.
% Images get their normal width if they fit onto the page, but
% are scaled down if they would overflow the margins.
\makeatletter
\def\ScaleWidthIfNeeded{%
 \ifdim\Gin@nat@width>\linewidth
    \linewidth
  \else
    \Gin@nat@width
  \fi
}
\def\ScaleHeightIfNeeded{%
  \ifdim\Gin@nat@height>0.9\textheight
    0.9\textheight
  \else
    \Gin@nat@width
  \fi
}
\makeatother

\setkeys{Gin}{width=\ScaleWidthIfNeeded,height=\ScaleHeightIfNeeded,keepaspectratio}%

$if(title)$
\title{\bigskip \bigskip $title$}
$endif$ 

%\author{$for(author)$$author$$sep$\\$endfor$}

\author{$for(author)$\Large $author.name$\vspace{0.05in} \\ \normalsize\emph{$author.affiliation$} \\ \footnotesize \vspace*{0.2in}\\ $sep$ \and $endfor$}

%\author{$for(author)$$author.name$ ($author.affiliation$)$sep$ \and $endfor$}

\date{$date$}
\linespread{1.5}

\begin{document}  
		
\begin{titlepage}
$if(title)$
\maketitle
$endif$

$if(abstract)$

\begin{abstract}

\noindent $abstract$

$if(keywords)$
\smallskip
\noindent \textbf{Keywords.} $keywords$
$endif$

\end{abstract}

$endif$

\end{titlepage}


$body$

$if(biblatex)$

\printbibliography$if(biblio-title)$[title=References]

$endif$

$endif$

\end{document}